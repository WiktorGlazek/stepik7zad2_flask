**Corporation api project**

**Endpoints:**

**get all employees -->** **GET** 
/employees

**get but with optional parameters -->** **GET** 
/employees?filter=(...)&value=(...)&sort=(...)

**add new employee -->** **POST** 
/employees

**edit employee propperties by id -->** **PUT** 
/employees/\<id\>

**delete employee by id -->** **DELETE** 
/employees/\<id\>

**get subordinates of a manager by managers id -->** **GET** 
/employees/\<id\>/subordinates

**get employees of a department by department id -->** **GET** 
/departments/\<id\>/employees