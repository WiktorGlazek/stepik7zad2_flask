from flask import Flask, request, jsonify
from neo4j import GraphDatabase, basic_auth
from neo4j._async.driver import AsyncGraphDatabase
import re

app = Flask(__name__)

USERNAME="neo4j"
PASSWORD="test1234"
URI="bolt://localhost:7687"

app = Flask(__name__)

driver = AsyncGraphDatabase.driver(
    URI, auth=basic_auth(USERNAME, PASSWORD)
)

class RequestFailedError(Exception):
    pass

@app.route("/employees", methods=["GET"])
async def getAllEmployees():
    async with driver.session() as session:

        filter = request.args.get("filter")
        value = request.args.get("value")
        sort = request.args.get("sort")

        if filter and value:
            if re.match("[1-9][0-9]" , value) is not None:
                print("int")
                filterPart = f'where n.{filter}={value}'
            else:
                filterPart = f'where n.{filter}="{value}"'
        else:
            filterPart = ''
        if sort:
            sortPart = f'order by n.{sort}'
        else:
            sortPart = ""

        query = f"match (n:Employee) {filterPart} return n {sortPart}"

        try:
            db_response = await session.run(query)
            data = await db_response.data()
            await session.close()
        except:
            raise RequestFailedError
        return data


@app.route("/employees", methods=["POST"])
async def addNewEmployee():

    person = request.get_json()
    name = person["name"]
    lastname = person["lastname"]
    age = person["age"]
    department = person["department"]
    position = person["position"]

    async with driver.session() as session:
        query = "merge (n:Employee {name:$name,lastname:$lastname}) merge (d:Department {name:$department})merge (n)-[:WORKS_IN]->(d)"
        try:
            await session.run(
                query,
                name=name,
                lastname=lastname,
                age=age,
                department=department,
                position=position,
            )
            await session.close()
            return "Employee created"
        except:
            raise RequestFailedError

@app.route("/employees/<id>", methods=["PUT"])
async def editEmployee(id):

    person = request.get_json()
    name = person["name"]
    lastname = person["lastname"]
    age = person["age"]
    department = person["department"]
    position = person["position"]

    async with driver.session() as session:
        query = f"match (n:Employee) where ID(n) = {id} set n.name = '{name}', n.lastname = '{lastname}', n.age = {age}, n.department = '{department}', n.position = '{position}'"
        try:
            await session.run(query)
            await session.close()
            return "Employee updated"
        except:
            raise RequestFailedError

@app.route("/employees/<id>", methods=["DELETE"])
async def deleteEmployee(id):
    async with driver.session() as session:
        query = f"match (n:Employee) where ID(n) = {id} detach delete n"
        try:
            await session.run(query)
            await session.close()
            return "Employee deleted"
        except:
            raise RequestFailedError

@app.route("/employees/<id>/subordinates", methods=["GET"])
async def getSubordinates(id):
    async with driver.session() as session:
        try:
            query = f"match (manager:Employee)-[:MANAGES]->(:Department)<-[:WORKS_IN]-(sub:Employee) where ID(manager) = {id} and ID(sub) <> {id} return sub"
            db_response = await session.run(query)
            data = await db_response.data()
            await session.close()
            return data
        except:
            raise RequestFailedError

@app.route("/departments/<id>/employees", methods=["GET"])
async def getSubordinatesByDepartment(id):
    async with driver.session() as session:
        try:
            query = f"match (n:Employee)-[:WORKS_IN]->(d:Department) where ID(d) = {id} return n"
            db_response = await session.run(query)
            data = await db_response.data()
            await session.close()
            return data
        except:
            raise RequestFailedError